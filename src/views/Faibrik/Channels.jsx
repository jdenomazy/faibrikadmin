import React from "react";
import {
  Table,
  UncontrolledTooltip,
  ButtonGroup,
  FormGroup,
  Label,
  Input,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";

import { PanelHeader, Button } from "components";

import jacket from "assets/img/saint-laurent.jpg";
import shirt from "assets/img/balmain.jpg";
import swim from "assets/img/prada.jpg";

const ChannelHeader = ["ID", "Channel", "Type", "Language", "Environment", "Status"];

class Channels extends React.Component {
  state = {
    ChannelData:[]
  }

  componentDidMount(){
    this.getChannels();
  }
  
getChannels = _ => {
    fetch('http://localhost:3999/channels')
      .then(response => response.json())
      .then(response => this.setState({ ChannelData: response }))
      .catch(err => console.error(err))
  }

  render() {
    var {ChannelData} = this.state;
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Channels</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                      <tr className="text-primary">
                        {ChannelHeader.map((prop, key) => {
                          return <th className="text-left">{prop}</th>;
                        })}
                        <th className="text-right">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    {ChannelData.map((prop, key) => {
                        return (
                          <tr key={key}>
                            {prop.map((prop, key) => {
                                return (
                                  <td key={key} className="text-left">
                                    {prop}
                                  </td>
                                );
                            })}
                            <td className="text-right">
                          <Button icon color="success" size="sm">
                            <i className="now-ui-icons ui-2_settings-90" />
                          </Button>{" "}
                          <Button icon color="danger" size="sm">
                            <i className="now-ui-icons ui-1_simple-remove" />
                          </Button>
                          </td>
                          </tr>
                        );
                      })}
                    
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
            <Col xs={12}>
              
            </Col>
            <Col xs={12}>
              
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Channels;
