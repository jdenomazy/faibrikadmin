import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

import {
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import { PanelHeader, Timeline } from "components";

import { widgetStories } from "variables/general";

import Button from "components/CustomButton/CustomButton.jsx"; 

import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import logofile from "assets/img/flags/FR.png";
import { array } from "prop-types";

// ##############################
// // // data for calling orchestrator routes
// #############################


const orc_url = "https://faibrik-orc.eu-gb.mybluemix.net"; //fAIbrik DEV
//const orc_url = "https://laClusaz-orchestrator.eu-gb.mybluemix.net"; //LC TEST
//const orc_url = "https://bsharpe-orchestrator.eu-gb.mybluemix.net"; //BS TEST


const convdetail_route = orc_url+"/detailConv";
const sendphrase_route=orc_url+"/sendPhrase";
const updateconvstatus_route=orc_url+"/updateConvStatus";


var thePhrase='';

function GetLogoButton(logotype, logovalue) {
  if (logotype=='channel') {
    if (logovalue=="FB") {return(<Button icon round facebook><i className="fa fa-facebook"> </i></Button>)}
    else if (logovalue=='WEB') {return(<Button icon round color="primary">w</Button>)}
    else return(logovalue)
  }
  else if (logotype=='status') {
    if (logovalue=='active') {return(<Button color="success">Active</Button>)}
    else if (logovalue=='humanrqst') {return(<Button color="primary">Human Request</Button>)}
    else if (logovalue=='blocked') {return(<Button color="danger">Blocked</Button>)}
    else if (logovalue=='inactive') {return(<Button color="warning">Inactive</Button>)}
    else if (logovalue=='archived') {return(<Button >Archived</Button>)}
    else  {return(<Button color="info">logovalue</Button>)}
  }
  else if (logotype=='language') {
    return (<div className="logo-img"><img src={logofile} alt={logovalue}/></div>)
  }
  else return(logovalue)
}

class ConversationDetails extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      error: null,
      isLoaded: false,
      conv_id: props.conv_id,
      ConversationData:[],
      data:[],
      Conversation:[],
      RecordKey: null,
      ConvDetail:[],
      ConvHistory:[],
      thePhraseform:"",
      isHidden:true
    };
  }
  
  SetPhrase (e){
    thePhrase=e.target.value;
    this.setState({
      thePhraseform:e.target.value
  });
  }

  SendPhrase(){
    if (thePhrase=='') {return};
    if (this.props.conversation.conversation_uid==null) {return};
    const sendPhrase_body = new URLSearchParams();
    sendPhrase_body.append('text',thePhrase);
    sendPhrase_body.append('convID',this.props.conversation.conversation_uid);

    var request = new Request(sendphrase_route, {
      method: 'POST', 
      body: sendPhrase_body.toString(), 
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      } 
  });
    fetch(request)
    .then(response => response.json())

    this.ClearPhrase();

    //this.ReadConversationHistory(this.props.conversation.conversation_uid);
    
}

ClearPhrase(){
  this.setState({
    thePhraseform:""
});
}

UpdateConvStatus(newstatus){
  if (this.props.conversation.conversation_uid==null) {return};
  const updateConvStatus_body = new URLSearchParams();
  updateConvStatus_body.append('status',newstatus);
  updateConvStatus_body.append('convID',this.props.conversation.conversation_uid);

  var request = new Request(updateconvstatus_route, {
    method: 'POST', 
    body: updateConvStatus_body.toString(), 
    headers:{
      'Content-Type': 'application/x-www-form-urlencoded'
    } 
});
  fetch(request)
  .then(response => response.json())

}

async componentDidMount() {
  try {
    setInterval(async () => {
      if (this.props.conversation.conversation_uid!=null){
        const convdetail_body = new URLSearchParams();
        convdetail_body.append('data',this.props.conversation.conversation_uid);
        var request = new Request(convdetail_route, {
            method: 'POST', 
            body: convdetail_body.toString(), 
            headers:{'Content-Type': 'application/x-www-form-urlencoded'} 
        });
        //fetch the request
        const response = await fetch(request);
        const result= await response.json();
        this.setState({
          ConvDetail:result,
          isHidden:false
        });
        //split the phrases
        var i;
        var j=0;
        var convarray= [];
        for (i=0; i< this.state.ConvDetail.length;i++) {
          var temparray= [];
          if ((this.state.ConvDetail[i][3]!="") && (this.state.ConvDetail[i][3]!=null)) {
            convarray[j]=[this.state.ConvDetail[i][0], "visitor", this.state.ConvDetail[i][2], this.state.ConvDetail[i][3]];
            j++;
          };
          if ((this.state.ConvDetail[i][4]!="") && (this.state.ConvDetail[i][4]!=null)) {
            convarray[j]=[this.state.ConvDetail[i][0], "bot", this.state.ConvDetail[i][2], this.state.ConvDetail[i][4]];
            j++;
          };
          if ((this.state.ConvDetail[i][5]!="") && (this.state.ConvDetail[i][5]!=null)) {
            convarray[j]=[this.state.ConvDetail[i][0], "agent", this.state.ConvDetail[i][2], this.state.ConvDetail[i][5]];
            j++;
          };
        } 
        this.setState({
          ConvHistory: convarray,
        });
      }
    }, 5000);
  } 
  catch(error) {
    this.setState({
    isLoaded: false,
    error
    });
  }
}


  
  render() {
    if (this.state.isHidden==true) {return(<Card></Card>)};
    return (
              <Card >
              <CardHeader>
                  <CardTitle tag="h4">Conversation: {this.props.conversation.conversation_uid} - {this.props.conversation.status} - {this.props.conversation.channel} - {this.props.conversation.language}</CardTitle>
                  <div className="btns-mr-5">
                    <Button color="warning" leftLabel="now-ui-icons ui-1_check" onClick={() => {this.UpdateConvStatus("intercepted");}}>
                      Intercepter
                    </Button>
                    <Button  leftLabel="now-ui-icons ui-1_lock-circle-open" onClick={() => {this.UpdateConvStatus("archived");this.setState({isHidden:true});this.props.conversation.conversation_uid=null}}>
                      Archiver
                    </Button>
                    <Button color="danger" leftLabel="now-ui-icons ui-1_simple-remove" onClick={() => {this.UpdateConvStatus("blocked");}}>
                      Bloquer
                    </Button>
                    <Button color="success" leftLabel="now-ui-icons ui-1_simple-remove" onClick={() => {this.UpdateConvStatus("active");}}>
                      Activer
                    </Button>
                  </div>
                </CardHeader>
                <CardBody>
              <VerticalTimeline animate={false}>
              {this.state.ConvHistory.map((prop) => {
                var textpos;
                var iconcolor;
                if (prop[1]=="visitor") {textpos="left"; iconcolor='blue'} 
                else if  (prop[1]=="bot") {textpos="right"; iconcolor='green' }
                else if  (prop[1]=="agent") {textpos="right"; iconcolor='orange' };
                var mydate=new Date(prop[2]);
                return (
                  <VerticalTimelineElement
                  className="vertical-timeline-element--work"
                  date={mydate.getHours()+':' + mydate.getMinutes()+':'+ mydate.getSeconds()}
                  iconStyle={{ background: iconcolor, color: {iconcolor} }} 
                  position={textpos}
                  >
                  <p>
                  {prop[3]}
                </p>
                </VerticalTimelineElement>
                );
                }) 
              }           
              </VerticalTimeline>
              <Row>
                    <Label sm={2}>Entrez votre message :</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            onChange={e => this.SetPhrase(e)}
                            value={this.state.thePhraseform}
                          />
                        </FormGroup>
                      </Col>
                      <Col>
                      <Button onClick={() => {
                      this.SendPhrase();
                  }}>Envoyer</Button>
                      
                      <Button onClick={() => {
                      this.ClearPhrase();
                  }}>Annuler</Button>

                  </Col>
                    </Row>
              </CardBody>
              </Card>
    );
  }
}

export default ConversationDetails;
