import React from "react";
import {
  Table,
  UncontrolledTooltip,
  ButtonGroup,
  FormGroup,
  Label,
  Input,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";

import { PanelHeader, Button } from "components";

import jacket from "assets/img/saint-laurent.jpg";
import shirt from "assets/img/balmain.jpg";
import swim from "assets/img/prada.jpg";

class Univers extends React.Component {
  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Select your content Universes</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table responsive striped>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-center">#</th>
                        <th className="text-center" />
                        <th>Name</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td className="text-center">1</td>
                        <td className="text-center">
                          <FormGroup check>
                            <Label check>
                              <Input type="checkbox" defaultChecked />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        </td>
                        <td>Basic</td>
                        <td>Bot control : greetings, thanks, call, human interaction, insults, ...</td>
                      </tr>
                      <tr>
                        <td className="text-center">2</td>
                        <td className="text-center">
                          <FormGroup check>
                            <Label check>
                              <Input type="checkbox" defaultChecked />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        </td>
                        <td>Meteo</td>
                        <td>Weather Forecast</td>
                      </tr>
                      <tr>
                        <td className="text-center">3</td>
                        <td className="text-center">
                          <FormGroup check>
                            <Label check>
                              <Input type="checkbox" defaultChecked />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        </td>
                        <td>Meteo</td>
                        <td>Weather Forecast</td>
                      </tr>
                      <tr>
                        <td className="text-center">4</td>
                        <td className="text-center">
                          <FormGroup check>
                            <Label check>
                              <Input type="checkbox" defaultChecked />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        </td>
                        <td>Ski Resort</td>
                        <td>Snow Coverage, SkiPass, SkiClass</td>
                      </tr>
                      <tr>
                        <td className="text-center">5</td>
                        <td className="text-center">
                          <FormGroup check>
                            <Label check>
                              <Input type="checkbox" />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        </td>
                        <td>Appointment</td>
                        <td>Get and update appointments</td>
                      </tr>
                      <tr>
                        <td className="text-center">6</td>
                        <td className="text-center">
                          <FormGroup check>
                            <Label check>
                              <Input type="checkbox"  />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        </td>
                        <td>Event</td>
                        <td>Event dates, content and info</td>
                      </tr>
                    </tbody>
                    <tr>
                        <td className="text-center">7</td>
                        <td className="text-center">
                          <FormGroup check>
                            <Label check>
                              <Input type="checkbox" defaultChecked />
                              <span className="form-check-sign" />
                            </Label>
                          </FormGroup>
                        </td>
                        <td>Transport</td>
                        <td>Traffic, public transport info, parkings</td>
                      </tr>
                 </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
          <Col xs={12} md={6}>
                <CardBody>
                  <div className="btns-mr-5">
                    <Button leftLabel="now-ui-icons arrows-1_minimal-left">
                      Cancel
                    </Button>
                    <Button color="success" leftLabel="now-ui-icons ui-1_check">
                      Save
                    </Button>
                  </div>
                </CardBody>
              </Col>
            </Row>
        </div>
      </div>
    );
  }
}

export default Univers;
