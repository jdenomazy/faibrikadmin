import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

import {
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import Switch from "react-bootstrap-switch";

import { PanelHeader } from "components";

import Button from "components/CustomButton/CustomButton.jsx";



class AnswerItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      AnswerItemData:[],
      data:[],
      AnswerItem:[],
      RecordKey: null
    };
  }

  componentDidMount() {
    fetch('http://localhost:3999/answeritems')
    .then(response => response.json())
    .then(
      (result) => {
        this.setState({
          isLoaded:true,
          AnswerItemData:result,
          data:[]
        })
        this.setState({
          AnswerItem:this.state.AnswerItemData[0],
          RecordKey:0
        })
        this.setState({
          data: this.state.AnswerItemData.map((prop, key) => {
            return {
              id: key,
              answer_item_uid: prop[0],
              answer_uid: prop[1],
              name: prop[2],
              language: prop[3],
              channel: prop[4],
              text:prop[5],
              activated:prop[6],
              default:prop[7],
              last_update:prop[8],
              actions: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.data.find(o => o.id === key);
                      alert(
                        "You've clicked LIKE button on \n{ \nName: " +
                          obj.name +
                          ", \nposition: " +
                          obj.position +
                          ", \noffice: " +
                          obj.office +
                          ", \nage: " +
                          obj.age +
                          "\n}."
                      );
                    }}
                    color="info"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-heart" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      this.setState({AnswerItem:this.state.AnswerItemData[key], RecordKey:key
                    })
                  }}
                    color="warning"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-edit" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                  <Button
                    onClick={() => {
                      var data = this.state.data;
                      data.find((o, i) => {
                        if (o.id === key) {
                          // here you should add some custom code so you can delete the data
                          // from this component and from your server as well
                          data.splice(i, 1);
                          console.log(data);
                          return true;
                        }
                        return false;
                      });
                      this.setState({ data: data });
                    }}
                    color="danger"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-times" />
                  </Button>{" "}
                </div>
              )
            };
          })    
        })
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }
  
  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12} md={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">List of Answer Items</CardTitle>
                </CardHeader>
                <CardBody>
                  <ReactTable
                    data={this.state.data}
                    filterable
                    columns={[
                      {
                        Header: "Id",
                        accessor: "answer_item_uid"
                      },
                      {
                        Header: "Answer Code",
                        accessor: "name"
                      },
                      {
                        Header: "Language",
                        accessor: "language"
                      },
                      {
                        Header: "Channel",
                        accessor: "channel"
                      },
                      {
                        Header: "Text",
                        accessor: "text"
                      },
                      {
                        Header: "Actions",
                        accessor: "actions",
                        sortable: false,
                        filterable: false
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationTop
                    showPaginationBottom={false}
                    className="-striped -highlight"
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
          <Col md={12} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Answer Item Details</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                    <Label sm={2}>AnswerItem ID</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            disabled
                            placeholder={this.state.AnswerItem[0]}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>AnswerCode Name</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input 
                          type="text" 
                          placeholder={this.state.AnswerItem[1]} 
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>AnswerCode Description</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input 
                          type="text" 
                          placeholder={this.state.AnswerItem[2]} 
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>Language</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            placeholder={this.state.AnswerItem[3]}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                    <Label sm={2}>Channel</Label>
                    <Col xs={12} sm={10}>
                      <FormGroup>
                        <Input
                          type="text"
                          placeholder={this.state.AnswerItem[4]}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                  <Label sm={2}>Content</Label>
                  <Col xs={12} sm={10}>
                    <FormGroup>
                      <Input
                        type="text"
                        placeholder={this.state.AnswerItem[5]}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                      <Row>
                        <Col xs={12} md={4}>
                          <p className="category">Default</p>
                          <Switch state={this.state.AnswerItem[6]} /> <Switch />
                        </Col>
                        <Col xs={12} md={4}>
                          <p className="category">Active</p>
                          <Switch defaultValue={this.state.AnswerItem[7]} /> <Switch />
                        </Col>
                      </Row>
                    </Col>
                </Row>
                <Row>
                <Label sm={2}>Last Update</Label>
                <Col xs={12} sm={10}>
                  <FormGroup>
                    <Input
                      type="text"
                      disabled
                      placeholder={this.state.AnswerItem[8]}
                    />
                  </FormGroup>
                </Col>
              </Row>
                    <Row>
          <Col xs={12} md={6}>
                <CardBody>
                  <div className="btns-mr-5">
                    <Button leftLabel="now-ui-icons arrows-1_minimal-left"onClick={() => {
                      this.setState({AnswerItem:this.state.AnswerCodeItem[this.state.RecordKey]
                    })
                  }}>
                      Cancel
                    </Button>
                    <Button color="success" leftLabel="now-ui-icons ui-1_check"onClick={() => {
                      this.setState({AnswerItem:[]
                    })
                  }}>
                      Save
                    </Button>
                    <Button color="info" leftLabel="now-ui-icons ui-1_simple-remove" onClick={() => {
                      this.setState({AnswerItem:[]
                    })
                  }}>
                      New
                    </Button>
                  </div>
                </CardBody>
              </Col>
            </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}


export default AnswerItems;
