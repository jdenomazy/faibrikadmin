import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import debounce from 'lodash.debounce';

import {
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import { PanelHeader, Timeline } from "components";

import { widgetStories } from "variables/general";

import Button from "components/CustomButton/CustomButton.jsx";

import ConversationDetails from "views/Faibrik/ConversationDetail.jsx"

import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import logofile from "assets/img/flags/FR.png";
import { array } from "prop-types";

// ##############################
// // // data for calling orchestrator routes
// #############################

const orc_url = "https://faibrik-orc.eu-gb.mybluemix.net";

const conv_route = orc_url + "/listConv";
const conv_body = {
  data: 'true'
};


function GetLogoButton(logotype, logovalue) {
  if (logotype=='channel') {
    if (logovalue=="FB") {return(<Button icon round facebook><i className="fa fa-facebook"> </i></Button>)}
    else if (logovalue=='WEB') {return(<Button icon round color="primary">w</Button>)}
    else return(logovalue)
  }
  else if (logotype=='status') {
    if (logovalue=='active') {return(<Button color="success">Active</Button>)}
    else if (logovalue=='humanrqst') {return(<Button color="primary">Human Request</Button>)}
    else if (logovalue=='blocked') {return(<Button color="danger">Blocked</Button>)}
    else if (logovalue=='inactive') {return(<Button color="warning">Inactive</Button>)}
    else if (logovalue=='archived') {return(<Button >Archived</Button>)}
    else  {return(<Button color="info">logovalue</Button>)}
  }
  else if (logotype=='language') {
    return (<img src={logofile} alt={logovalue}/>)
  }
  else return(logovalue)
}

var Selected_conv={id:null, conversation_uid:null, status:null, last_update:null, channel:null, language:null};

class Conversations extends Component {
  constructor(props) {
    super(props);
    this.handleClickView = this.handleClickView.bind(this);
    this.emitClickViewDebounced = debounce(this.emitClickView,500);

    this.state = {
      error: null,
      isLoaded: false,
      ConversationData:[],
      data:[],
      Conversation:[],
      RecordKey: null,
      ConvDetail:[],
      Selected_conv:{id:null, conversation_uid:null, status:null, last_update:null, channel:null, language:null}
      
    };
    this.LoadConversationList()
  }
  
  componentWillUnmount() {
    this.emitClickViewDebounced.cancel();
  }
  
  handleClickView(e) {
    this.emitClickViewDebounced(e);
  }
  emitClickView(myconv) {
    Selected_conv= myconv;
    this.setState({
      Selected_conv: myconv
    })
  }

  componentDidMount() {
    this.LoadConversationList();
    setInterval(this.LoadConversationList, 5000);
  }
  
  LoadConversationList() {
    var request = new Request(conv_route, {
      method: 'POST', 
      body: conv_body, 
      headers: new Headers()
  });
    fetch(request)
    .then(response => response.json())
    .then(
      (result) => {
        this.setState({
          isLoaded:true,
          ConversationData:result,
          data:[]
        })
        this.setState({
          Conversation:this.state.ConversationData[0],
          RecordKey:0
        })
        this.setState({
          data: this.state.ConversationData.map((prop, key) => {
            return {
              id: key,
              conversation_uid: prop[0],
              status: GetLogoButton("status",prop[1]),
              last_update: prop[2],
              channel: GetLogoButton("channel",prop[3]),
              language:GetLogoButton("language",prop[4]),
              view: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      this.handleClickView(this.state.data[key])
                    }}
                    color="info"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-heart" />
                  </Button>
                </div>
              )
            };
          })    
        })
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }
  
  

  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12} md={6}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">List of Conversations</CardTitle>
                </CardHeader>
                <CardBody>
                  <ReactTable
                    data={this.state.data}
                    
                    columns={[
                      {
                        Header: "Id",
                        accessor: "conversation_uid"
                      },
                      {
                        Header: "Status",
                        accessor: "status"
                      },
                      {
                        Header: "Last Update",
                        accessor: "last_update"
                      },
                      {
                        Header: "Channel",
                        accessor: "channel"
                      },
                      {
                        Header: "Language",
                        accessor: "language"
                      },
                      {
                        Header: "View",
                        accessor: "view"
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationTop={false}
                    showPaginationBottom={false}
                    filterable={false}
                    sortable={false}
                    className="-striped -highlight"
                  />
                </CardBody>
              </Card>
            </Col>
            <Col>
            <ConversationDetails conversation={this.state.Selected_conv}></ConversationDetails>
            </Col>
            
          </Row>
          
        </div>
      </div>
    );
  }
}


export default Conversations;
