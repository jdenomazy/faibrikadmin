import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

import {
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import { PanelHeader } from "components";

import Button from "components/CustomButton/CustomButton.jsx";



class AnswerCodes extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      error: null,
      isLoaded: false,
      AnswerCodeData:[],
      data:[],
      AnswerCode:[],
      RecordKey: null
    };
  }
  
  componentDidMount() {
    fetch('http://localhost:3999/answercodes')
    .then(response => response.json())
    .then(
      (result) => {
        this.setState({
          isLoaded:true,
          AnswerCodeData:result,
          data:[]
        })
        this.setState({
          AnswerCode:this.state.AnswerCodeData[0],
          RecordKey:0
        })
        this.setState({
          data: this.state.AnswerCodeData.map((prop, key) => {
            return {
              id: key,
              answer_uid: prop[0],
              code: prop[1],
              description: prop[2],
              last_update: prop[3],
              actions: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      let obj = this.state.data.find(o => o.id === key);
                      alert(
                        "You've clicked LIKE button on \n{ \nName: " +
                          obj.name +
                          ", \nposition: " +
                          obj.position +
                          ", \noffice: " +
                          obj.office +
                          ", \nage: " +
                          obj.age +
                          "\n}."
                      );
                    }}
                    color="info"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-heart" />
                  </Button>{" "}
                  {/* use this button to add a edit kind of action */}
                  <Button
                    onClick={() => {
                      this.setState({AnswerCode:this.state.AnswerCodeData[key], RecordKey:key
                    })
                  }}
                    color="warning"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-edit" />
                  </Button>{" "}
                  {/* use this button to remove the data row */}
                  <Button
                    onClick={() => {
                      var data = this.state.data;
                      data.find((o, i) => {
                        if (o.id === key) {
                          // here you should add some custom code so you can delete the data
                          // from this component and from your server as well
                          data.splice(i, 1);
                          console.log(data);
                          return true;
                        }
                        return false;
                      });
                      this.setState({ data: data });
                    }}
                    color="danger"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-times" />
                  </Button>{" "}
                </div>
              )
            };
          })    
        })
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }
  
  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12} md={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">List of Answer Codes</CardTitle>
                </CardHeader>
                <CardBody>
                  <ReactTable
                    data={this.state.data}
                    filterable
                    columns={[
                      {
                        Header: "Id",
                        accessor: "answer_uid"
                      },
                      {
                        Header: "Answer Code",
                        accessor: "code"
                      },
                      {
                        Header: "Description",
                        accessor: "description"
                      },
                      {
                        Header: "Last Update",
                        accessor: "last_update"
                      },
                      {
                        Header: "Actions",
                        accessor: "actions",
                        sortable: false,
                        filterable: false
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationTop
                    showPaginationBottom={false}
                    className="-striped -highlight"
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
          <Col md={12} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Answer Code Details</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                    <Label sm={2}>ID</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            disabled
                            placeholder={this.state.AnswerCode[0]}
                            
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>Code Name</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input 
                          type="text" 
                          placeholder={this.state.AnswerCode[1]} 
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>Code Description</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input 
                          type="text" 
                          placeholder={this.state.AnswerCode[2]} 
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>Last Update</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            disabled
                            placeholder={this.state.AnswerCode[3]}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
          <Col xs={12} md={6}>
                <CardBody>
                  <div className="btns-mr-5">
                    <Button leftLabel="now-ui-icons arrows-1_minimal-left"onClick={() => {
                      this.setState({AnswerCode:this.state.AnswerCodeData[this.state.RecordKey]
                    })
                  }}>
                      Cancel
                    </Button>
                    <Button color="success" leftLabel="now-ui-icons ui-1_check"onClick={() => {
                      this.setState({AnswerCode:[]
                    })
                  }}>
                      Save
                    </Button>
                    <Button color="info" leftLabel="now-ui-icons ui-1_simple-remove" onClick={() => {
                      this.setState({AnswerCode:[]
                    })
                  }}>
                      New
                    </Button>
                  </div>
                </CardBody>
              </Col>
            </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default AnswerCodes;
