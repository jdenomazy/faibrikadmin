import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import debounce from 'lodash.debounce';

import {
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import { PanelHeader, Timeline } from "components";

import { widgetStories } from "variables/general";

import Button from "components/CustomButton/CustomButton.jsx";

import ConversationDetails from "views/Faibrik/ConversationDetail.jsx"

import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import logofile from "assets/img/flags/FR.png";
import { array } from "prop-types";
import Push from "push.js";

// ##############################
// // // data for calling orchestrator routes
// #############################


const orc_url = "https://faibrik-orc.eu-gb.mybluemix.net"; //fAIbrik DEV
//const orc_url = "https://laClusaz-orchestrator.eu-gb.mybluemix.net"; //LC TEST
//const orc_url = "https://bsharpe-orchestrator.eu-gb.mybluemix.net"; //BS TEST


const conv_route = orc_url + "/listConv";
const conv_body = {
  data: 'true'
};
const updateglobal_route=orc_url+"/changeGlobals"



var Selected_conv={id:null, conversation_uid:null, status:null, last_update:null, channel:null, language:null};
var HumanRqstNewFlag=false;
var HumanRqstFlag=false;
var LastConvID=0;

class Conversations extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      error: null,
      isLoaded: false,
      ConversationData:[],
      data:[],
      Conversation:[],
      RecordKey: null,
      ConvDetail:[],
      Selected_conv:{id:null, conversation_uid:null, status:null, last_update:null, channel:null, language:null}  ,
    };
    this.LoadConversationList()
  }
  
  handleClickView(myconv) {
    Selected_conv= myconv;
    this.setState({
      Selected_conv: myconv
    });
  }

  UpdateGlobals(key,value) {
    const updateGlobals_body = new URLSearchParams();
    updateGlobals_body.append(key,value);
    
    var request = new Request(updateglobal_route, {
      method: 'POST', 
      body: updateGlobals_body.toString(), 
      headers:{
        'Content-Type': 'application/x-www-form-urlencoded'
      } 
  });
    fetch(request)
    .then(response => response.json())
  
  }


 GetLogoButton(logotype, logovalue) {
  if (logotype=='channel') {
    if (logovalue=="FB") {return(<Button icon round facebook><i className="fa fa-facebook"> </i></Button>)}
    else if (logovalue=='WEB') {return(<Button icon round color="primary">w</Button>)}
    else return(logovalue)
  }
  else if (logotype=='status') {
    if (logovalue=='active') {return(<Button color="success">Active</Button>)}
    else if (logovalue=='humanrqst') {
      HumanRqstNewFlag=true;
      return(<Button color="primary">Human Request</Button>)
    }
    else if (logovalue=='blocked') {return(<Button color="danger">Blocked</Button>)}
    else if (logovalue=='inactive') {return(<Button color="warning">Inactive</Button>)}
    else if (logovalue=='archived') {return(<Button >Archived</Button>)}
    else if (logovalue=='intercepted') {return(<Button color="warning">Intercepted</Button>)}
    else  {return(<Button color="info">logovalue</Button>)}
  }
  else if (logotype=='language') {
    return (<img src={logofile} alt={logovalue}/>)
  }
  else return(logovalue)
}

  async componentDidMount() {
    try {
      setInterval(async () => {
        var request = new Request(conv_route, {
          method: 'POST', 
          body: conv_body, 
          headers: new Headers()
        });
        //alert("zou");
        const response = await fetch(request);
        const result= await response.json();
        HumanRqstNewFlag=false;
            this.setState({
              isLoaded:true,
              ConversationData:result,
              data:[]
            });
            this.setState({
              Conversation:this.state.ConversationData[0],
              RecordKey:0
            });
            this.setState({
              data: this.state.ConversationData.map((prop, key) => {
                return {
                  id: key,
                  conversation_uid: prop[0],
                  status: this.GetLogoButton("status",prop[1]),
                  last_update: prop[2],
                  channel: this.GetLogoButton("channel",prop[3]),
                  language: this.GetLogoButton("language",prop[4]),
                  view: (
                    // we've added some custom button actions
                    <div className="actions-right">
                      {/* use this button to add a like kind of action */}
                      <Button
                        onClick={() => {
                          this.handleClickView(this.state.data[key])
                        }}
                        color="info"
                        size="sm"
                        round
                        icon
                      >
                        <i className="fa fa-heart" />
                      </Button>
                    </div>
                  )
                };
              })    
            }); 
            if (this.state.data[0]) {
              // raise an alert if there is a new conversation
              if (LastConvID<this.state.data[0].conversation_uid) {
                LastConvID=this.state.data[0].conversation_uid;
                Push.create("Nouvelle Conversation", {
                  body: "Nouvelle Conversation",
                  icon: '/icon.png',
                  requireInteraction: true,
                  onClick: function () {
                    window.focus();
                    this.close();
                  }
                });
              }
              // raise an alert if there is a new conversation
              if ((HumanRqstNewFlag==false) && (HumanRqstFlag==true))  {
                HumanRqstFlag=false
              } else if ((HumanRqstNewFlag==true) && (HumanRqstFlag==false)) {
                HumanRqstFlag=true;
                Push.create("Conversation en attente", {
                  body: "Conversation en attente",
                  icon: '/icon.png',
                  requireInteraction: true,
                  onClick: function () {
                    window.focus();
                    this.close();
                  }
                });
                HumanRqstNewFlag=false
              };
            }   
      }, 5000);
    } catch(error) {
        this.setState({
        isLoaded: false,
        error
      });
    }
  }
  
  async LoadConversationList() {
    var request = new Request(conv_route, {
      method: 'POST', 
      body: conv_body, 
      headers: new Headers()
  });
    try {
    const response = await fetch(request);
    const result= await response.json();
        this.setState({
          isLoaded:true,
          ConversationData:result,
          data:[]
        });
        this.setState({
          Conversation:this.state.ConversationData[0],
          RecordKey:0,
          Selected_conv:Selected_conv
        });
        this.setState({
          data: this.state.ConversationData.map((prop, key) => {
            return {
              id: key,
              conversation_uid: prop[0],
              status: this.GetLogoButton("status",prop[1]),
              last_update: prop[2],
              channel: this.GetLogoButton("channel",prop[3]),
              language:this.GetLogoButton("language",prop[4]),
              view: (
                // we've added some custom button actions
                <div className="actions-right">
                  {/* use this button to add a like kind of action */}
                  <Button
                    onClick={() => {
                      this.handleClickView(this.state.data[key])
                    }}
                    color="info"
                    size="sm"
                    round
                    icon
                  >
                    <i className="fa fa-heart" />
                  </Button>
                </div>
              )
            };
          })    
        });
      } catch (error) {
        this.setState({
          isLoaded: false,
          error
        });
      }
  }


  render() {
    return (
      <div >
        <PanelHeader size="sm" />
        <div className="content">
          <Row class="h-100">
            <Col xs={12} md={6}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">List of Conversations</CardTitle>
                  <div className="btns-mr-5">
                  <Button color="success" leftLabel="now-ui-icons ui-1_lock-circle-open" onClick={() => {this.UpdateGlobals("human_agent_service","open");}}>
                      Open Customer Care
                    </Button>
                    <Button color="danger" leftLabel="now-ui-icons ui-1_simple-remove" onClick={() => {this.UpdateGlobals("human_agent_service","close");}}>
                      Close Customer Care
                    </Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <ReactTable
                    data={this.state.data}
                    
                    columns={[
                      {
                        Header: "Id",
                        accessor: "conversation_uid"
                      },
                      {
                        Header: "Status",
                        accessor: "status"
                      },
                      {
                        Header: "Last Update",
                        accessor: "last_update"
                      },
                      {
                        Header: "Channel",
                        accessor: "channel"
                      },
                      {
                        Header: "Language",
                        accessor: "language"
                      },
                      {
                        Header: "View",
                        accessor: "view"
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationTop={false}
                    showPaginationBottom={false}
                    filterable={false}
                    sortable={false}
                    className="-striped -highlight"
                  />
                </CardBody>
              </Card>
            </Col>
            <Col >
            <ConversationDetails conversation={this.state.Selected_conv}></ConversationDetails>
            </Col>
            
          </Row>
          
        </div>
      </div>
    );
  }
}


export default Conversations;
