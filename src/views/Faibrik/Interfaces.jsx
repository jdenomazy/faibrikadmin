import React from "react";
import {
  Table,
  UncontrolledTooltip,
  ButtonGroup,
  FormGroup,
  Label,
  Input,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";

import { PanelHeader, Button } from "components";

import jacket from "assets/img/saint-laurent.jpg";
import shirt from "assets/img/balmain.jpg";
import swim from "assets/img/prada.jpg";

const InterfaceHeader = ["ID", "Interface", "Description", "Language", "Environment", "Status"];

class Interfaces extends React.Component {
  state = {
    InterfaceData:[]
  }

  componentDidMount(){
    this.getInterfaces();
  }
  
getInterfaces = _ => {
    fetch('http://localhost:3999/interfaces')
      .then(response => response.json())
      .then(response => this.setState({ InterfaceData: response }))
      .catch(err => console.error(err))
  }

  render() {
    var {InterfaceData} = this.state;
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">interfaces</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table responsive>
                    <thead>
                      <tr className="text-primary">
                        {InterfaceHeader.map((prop, key) => {
                          return <th className="text-left">{prop}</th>;
                        })}
                        <th className="text-right">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    {InterfaceData.map((prop, key) => {
                        return (
                          <tr key={key}>
                            {prop.map((prop, key) => {
                                return (
                                  <td key={key} className="text-left">
                                    {prop}
                                  </td>
                                );
                            })}
                            <td className="text-right">
                          <Button icon color="success" size="sm">
                            <i className="now-ui-icons ui-2_settings-90" />
                          </Button>{" "}
                          <Button icon color="danger" size="sm">
                            <i className="now-ui-icons ui-1_simple-remove" />
                          </Button>
                          </td>
                          </tr>
                        );
                      })}
                    
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
            <Col xs={12}>
              
            </Col>
            <Col xs={12}>
              
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Interfaces;
